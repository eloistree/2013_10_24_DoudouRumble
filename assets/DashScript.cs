﻿using UnityEngine;
using System.Collections;

public class DashScript : MonoBehaviour {
	

	private float duration=0.5f;
	private float velocity=20.0f;
	private Vector3 originie		= new Vector3();
	private Vector3 destination	= new Vector3();
	private bool start;
	public float timeStart;
	private float dashForce=0.9f;
	
	void Start()
	{
		float time = Time.timeSinceLevelLoad;
		timeStart= time;
	}
	// Update is called once per frame
	void Update () {
	
		float time = Time.timeSinceLevelLoad;
		float timeSinceStart= time-timeStart;
		if(start)
		{
			if(time-timeStart>duration )
			{
				Destroy(this);
			}
			
			Vector3 dir= destination-originie;
			dir.Normalize();
			
			//this.gameObject.transform.Translate(dir*Time.deltaTime* velocity);
			this.gameObject.rigidbody.AddForce(dir*dashForce,ForceMode.VelocityChange);
		}
	}
	
	
	
	public void SetOrigine(Vector3 val)
	{
		val.y= this.gameObject.transform.position.y;
		originie= val;
		
	}
	public void SetDest(Vector3 val)
	{
		
		val.y= this.gameObject.transform.position.y;
		destination=val;
	}
	
	public void Play(){start=true;}
}
