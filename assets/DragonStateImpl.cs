﻿using UnityEngine;
using System.Collections;

public class DragonStateImpl : UnityState {
	
	
	public Weapon_Sword swordWeapon;
	public Weapon_Bow bowWeapon;
	public float fireBallDelay =1.2f;
	public float dommage=20;
	public Animator animator;
	
	public void Start()
	{
		base.AnimName_Rest="Rest";
		base.AnimName_Running="Move";
		base.AnimName_SwordStrike="";
		base.AnimName_BowStrike="Attack";
		base.AnimName_Dying="";
		base.AnimName_Death="Death";
		
		
		bowWeapon= this.gameObject.GetComponentInChildren<Weapon_Bow>();
		swordWeapon= this.gameObject.GetComponentInChildren<Weapon_Sword>();
		
		animator =	animationOwner.GetComponent<Animator>();
		
			InGame.GetInstance().boss=this.gameObject;
		
	}
	
	public override void OnHitTaken()
	{
		//to change by dragon sound 
		InGame.GetInstance().sound.Play(SoundManager.SoundType.EnemyTakeHit,this.gameObject,true);	
		InGame.GetInstance().fx.Play(FXManager.FXType.EnemyTakeHit,this.gameObject,true);		
	}
	

	
	
	public override void OnStartRunning()
	{
		PlayAnimation(AnimName_Running,true);
		
	}
	public override void OnRunning()
	{
		PlayAnimation(AnimName_Running,true);
		
	}
	public override void OnEndRunning()
	{
		PlayAnimation(AnimName_Running,false);
		
	}
	
	
	public override void OnStartTakingRest()
	{
		
		PlayAnimation(AnimName_Rest,true);
		
	}
	public override void OnTakingRest()
	{
		
	}
	public override void OnEndTakingRest()
	{
		
		PlayAnimation(AnimName_Rest,false);
	}
	
	
	public override void OnStartDying()
	{
		
		PlayAnimation(AnimName_Dying,true);
		
	}
	public override void OnDying()
	{
		
	}
	public override void OnEndDying()
	{
		PlayAnimation(AnimName_Dying,false);
	}
	
	
	public override void OnDeath()
	{
		
	
		PlayAnimation(AnimName_Death,true);
		DragonStateImpl st = GetComponent<DragonStateImpl>();
		
	
		//	print ("I am the boooooossssss");
			InGame.GetInstance().fx.Play(FXManager.FXType.Bossdeath,this.gameObject,false);
		//InGame.GetInstance().sound.Play(SoundManager.SoundType.EnemyDeath,this.gameObject,true);
			InGame.GetInstance().bossDeath=true;
			InGame.GetInstance().boss=null;
		
	
	}
	
	
	public override void OnStartSwordStriking()
	{
		//swordWeapon.SwordStrike();
		PlayAnimation(AnimName_SwordStrike,true);
		
		
	}
	public override void OnSwordStriking()
	{
		
		if(!animationOwner.animation.IsPlaying(AnimName_SwordStrike))
		{
			PlayAnimation(AnimName_SwordStrike,true);
		}
	}
	public override void OnEndSwordStriking()
	{
	
			//swordWeapon.EndOfStrike();
			PlayAnimation(AnimName_SwordStrike,false);
	}
	
	public override void OnStartBowStriking()
	{
		bowWeapon.ShootArrow(Time.timeSinceLevelLoad,fireBallDelay);
		
		PlayAnimation(AnimName_BowStrike,true);
	}
	public override void OnBowStriking()
	{
			
		if(!animationOwner.animation.IsPlaying(AnimName_BowStrike))
		{
			PlayAnimation(AnimName_BowStrike,true);
		}
	}
	public override void OnEndBowStriking()
	{
		
			PlayAnimation(AnimName_BowStrike,false);
	}
	
	
	private void PlayAnimation(string anim, bool val)
	{
		if(animator!=null)
		{
			animator.SetBool(anim,val);
		}
	}
	
	public  override  bool IsSwordStriking()
	{
		return false;
	}
			public override bool IsBowFirering()
		{
	
			return animator.GetBool(AnimName_BowStrike);
		}
	
	protected override bool IsPlaying(string anim)
	{
		return animator.GetBool(anim);
		
	}
	
	
	
	public bool IsAbleToAttack()
	{
		if(animator==null) return false;
		
		return  animator.GetCurrentAnimatorStateInfo(0).IsTag(base.AnimName_Rest)  || animator.GetCurrentAnimatorStateInfo(0).IsTag(base.AnimName_Running) ;
	}
	
	

}
