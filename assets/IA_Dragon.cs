﻿using UnityEngine;
using System.Collections;

public class IA_Dragon : MonoBehaviour {
	
	
	
	public enum AttackType{Ball,Balls}


	
	
	
		/**The object that the IA will try to kill*/
	public static GameObject[] objectifs ;
	/**The player object*/
	public static GameObject player;
	
	/**The current target that the AI is going to kill*/
	public GameObject target;
	public GameObject initTarget;
	private NavMeshAgent agent;
	public  GameObject modele3D;
	private DragonStateImpl state ;
	
	private static GameObject [] pathPoints;
	private GameObject destination;
	
	/**Time to the AI to check again whom he must attack*/
	public float refreshDelay=0.5f;
	
	/**Speed for a attack to be effectively done*/
	public float attackSpeed=3f;
	/* Las time the IA attacked*/
	private float lastAttack;
	/**Last time the IA checked the objectif */
	private float lastRefresh;
	
	/*The minimum time between a attack, must be higher then attack speed*/
	public float attackDelay=1f;
	public float delayBeforeAttack=0.3f;
	
	/**The radius where the player need to be to aggro the mob*/
	public float radiusPlayerDetection=40f;
	/** The radus needed for the IA mob to attack the target*/
	public float radiusAttack=25f;
	
	private bool bossBehavior;
	public bool isAttacking;
	
	private float timeStartAttacking;
	
	 void Start()
	{
		//Set the objectif with the tag
		IA_Dragon.objectifs = GameObject.FindGameObjectsWithTag(InGame.TagName_Objectif);
		
		//Set the objectif with the tag
		IA_Dragon.pathPoints = GameObject.FindGameObjectsWithTag(InGame.TagName_DragonPath);
		
		//Set the player objectif with a tag
		IA_Dragon.player = InGame.GetInstance().player.GetPlayer();
		//Set the agent that will lead the object
		agent = GetComponent<NavMeshAgent>();	
		state = GetComponent<DragonStateImpl>();
		
		RefreshTarget();
		if(target!=null){
		agent.destination= target.transform.position;
		agent.stoppingDistance= radiusAttack-0.5f;
		}
		oldSpeedTmp=agent.speed;
		
	
		initTarget = IA_Dragon.objectifs[Random.Range(0,IA_Dragon.objectifs.Length)];
		
		
		UnityState us = this.GetComponent<UnityState>();
		if(us!=null)
			us.delayAttack =	delayBeforeAttack;
	
	}
	
	
	public void Update()
		{	
			float time = Time.timeSinceLevelLoad;
			
			if(state!=null && !state.IsItDeath()){
			//Regulary check the objectif
			if(time-lastRefresh>refreshDelay)
			{
				lastRefresh=time;
				RefreshTarget();
				if(state!=null)			
					if(!isAttacking && target==null){
						state.Rest();
					}
					else if(!isAttacking && target!=null)
					{
						if(IsDistanceOK())
						{
							state.Rest();
						}
						else
						{
							state.Move();
						}
					}
				
				
			}
			if(IsTargetAttackable() )
			{
			
					StartCoroutine(ReflexionTime(target));	
				
				
			}
		
				FixeTheEnemyAndLookTarget();
	
		}

	
		
		
		
		
	}
	public float oldSpeedTmp;
	private Vector3 v3TmpTarget = new Vector3 ();
	
	private float velocity=0.03f;
	//public void OnAnimation
	private void 	FixeTheEnemyAndLookTarget(){
		if(modele3D!=null && v3TmpTarget!=null&& target!=null){
			
			v3TmpTarget.x+= (target.transform.position.x-v3TmpTarget.x)*velocity;
			v3TmpTarget.z+= (target.transform.position.z-v3TmpTarget.z)*velocity;
			v3TmpTarget.y= 0;
			
			this.modele3D.transform.LookAt(v3TmpTarget);
		}
	}
	
	private float hitTime;
	
	public IEnumerator ReflexionTime(GameObject o){
		
		UnityState us = this.GetComponent<UnityState>();
		if(us!=null)
		us.timeStartAttacking =	Time.timeSinceLevelLoad;
	
		state.Rest();
		if(o!=null){
			isAttacking= true;
		yield return new WaitForSeconds(delayBeforeAttack);
		StartCoroutine(TryToAttack(o));}
	}
	
	public IEnumerator TryToAttack(GameObject o)
	{
		
		//If we try to attack a object
		if(o!=null){
			isAttacking= true;
			
			//The mob charge the attack
			yield return new WaitForSeconds(attackSpeed);
		if(state!=null)
			state.StikeWithBow();
			
			isAttacking=false;
		}
	}
	public bool IsDistanceOK()
	{
		return IsDistanceOK(target);
		
	}
	public bool IsDistanceOK(GameObject o){if(o==null)return false;

		bool distanceOk;
		if(target.Equals(player))
			distanceOk = Vector3.Distance(o.transform.position, this.transform.position)<radiusAttack;
		else distanceOk = Vector3.Distance(o.transform.position, this.transform.position)<0.1f;

	return distanceOk;}
	
	/**The target is attackable if it is in the attack range of the IA */
	public bool IsTargetAttackable()
	{
		return IsTargetAttackable(target);
		
	}
	
	
	/**The target is attackable if it is in the attack range of the IA */	
	public bool IsTargetAttackable(GameObject o)
	{
		bool isPlayer = target==player;
		bool distanceOk=IsDistanceOK(o);
		bool stateReady=false;
			if(state!=null)stateReady=state.IsAbleToAttack();
		
		//print ("Is Attackable: "+(!isAttacking && distanceOk && stateReady)+"          A: "+!isAttacking+"D: "+distanceOk+"S:"+stateReady);
		return !isAttacking && distanceOk && stateReady&&isPlayer;
		
	}

	/** Ask to look if there is not a better target in the objectif to kill*/
	public void RefreshTarget(){
		if(state!=null && !state.IsItDeath()){
			
			GameObject selection =null;
		
			
			if(player!=null && Vector3.Distance(player.transform.position,this.transform.position)<radiusPlayerDetection)
			{
				selection = player;
			}
				
			
			
			//if no destination or arrived to destination -> choose a new one
			if(  selection==null && destination==null)
			{
				destination= IA_Dragon.pathPoints[Random.Range(0,IA_Dragon.pathPoints.Length)];
				selection=destination;
			}
			 if(target!=null&& destination!=null && Vector3.Distance(this.transform.position,destination.transform.position)<3.5f)
			{
				destination=null;
			}
				
//			if(selection==null)
//			{
//				selection=player;
//			}
		
		
			
			// Set the target 
			if(selection!=null && agent!=null && agent.enabled ){
				target =selection;
				agent.destination= target.transform.position;
				if(target==player){agent.stoppingDistance= radiusAttack;}
				else{agent.stoppingDistance= 0;}
				
			}
		}
	}
	
}
