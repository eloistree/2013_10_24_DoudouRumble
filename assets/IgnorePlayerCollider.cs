﻿using UnityEngine;
using System.Collections;

public class IgnorePlayerCollider : MonoBehaviour {
	
	
	public Collider IgnoreCollider;
	void Start () {
		Physics.IgnoreCollision(this.gameObject.transform.collider,IgnoreCollider);
	}
	
}
