﻿using UnityEngine;
using System.Collections;

public class TutorialHUDScript : MonoBehaviour {
	
	public Texture2D txTuto;
	public string text="";
	
	private Rect centreTx=new Rect();
	public  Rect rectButton=new Rect(200,200,200,85); 
	public bool rectCenter=true;
	
	void Start () {
			Screen.showCursor=true;
		if(txTuto!=null){
			centreTx.width=txTuto.width;
			centreTx.height=txTuto.height;
		}
		centreTx.x=Screen.width/2- centreTx.width/2;
		centreTx.y=Screen.height/2- centreTx.height/2;
		if(rectCenter){
			rectButton.x= centreTx.width/2- rectButton.width/2;
		}
	}
	
	void OnGUI()
	{
		
			Time.timeScale=0;
			Screen.showCursor=true;
		if(txTuto!=null){
			GUI.DrawTexture(centreTx,txTuto);
			GUI.BeginGroup(centreTx);
			if(GUI.Button(rectButton,text))
			{
					Time.timeScale=1;

			Screen.showCursor=false;
			 	Destroy(this.gameObject);
				
			}
			GUI.EndGroup();
		
				
		}
	}
}
