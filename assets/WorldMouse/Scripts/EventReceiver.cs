﻿using UnityEngine;
using System.Collections;

public class EventReceiver : MonoBehaviour {
	public bool debugLog = true;
	public GameObject target;
	public bool enableHover = false;
	public bool enableClick = false;
	public bool enablePress = false;
	
	void OnEnable()
	{
		target.SetActive(false);
	}
	
	void WorldMouseOnHover(bool flag)
	{
		if(enableHover)
			target.SetActive(flag);
		
		if(debugLog)
			Debug.Log("WorldMouseOnHover event detected on object: " + gameObject.name);
	}
	
	void WorldMouseOnLeftClick()
	{
		if(enableClick)
			target.SetActive(!target.activeSelf);
		
		if(debugLog)
			Debug.Log("WorldMouseOnLeftClick event detected on object: " + gameObject.name);
	}
	
	void WorldMouseOnLeftPress()
	{
		if(enablePress)
			target.SetActive(!target.activeSelf);
		
		if(debugLog)
			Debug.Log("WorldMouseOnLeftClick event detected on object: " + gameObject.name);
	}
}
