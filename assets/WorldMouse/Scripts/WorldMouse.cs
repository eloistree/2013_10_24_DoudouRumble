//-------------------------------
//     World Mouse Script
// Copyright © 2013 GamiGen LLC
//-------------------------------
using UnityEngine;
using System.Collections;

public class WorldMouse : MonoBehaviour {
	#region PRIVATE VARIABLES
	Transform myTransform;
	Transform hitTransform;
	Transform lastHitTransform;
	bool leftmouseClicked = false;
	bool rightmouseClicked = false;
	bool leftmousePressed = false;
	bool rightmousePressed = false;
	#endregion
	
	#region PUBLIC VARIABLES - Mouse Configuration
	public float refreshRate = 0.5f;
	public bool centerMouseOnCamera = false; // Is the mouse positioned at center of the camera's view or free floating(false is free floating)?  
	public bool 		fixedToOwnerHeight;
	public GameObject 	owner;
	#endregion
	
	#region PUBLIC VARIABLES - Dependent Object References
	public Transform MouseRootObject;
	public Renderer MouseRenderer;
	public LayerMask MouseInteractionLayers;
	public float distanceFromPlayerDefault = 5; // How far should the cursor appear if there's no collision with the ray?
	public float maxReach = 5; // How far should the cursor appear if there's no collision with the ray?
	public Material InteractiveObjectMaterial;
	public Material NothingOfInterestMaterial;
	#endregion
	
	#region STATICS
	public static Ray ray;
	public static RaycastHit hit;
	public static Vector3 aimingPoint;
	public static Camera mouseCam;
	#endregion
	
	#region MONOBEHAVIOURS
	void Start () {
		myTransform = transform;
		Screen.showCursor = false;
		
		if(MouseRootObject != null && MouseRenderer == null)
			MouseRenderer = (Renderer)MouseRootObject.GetComponentInChildren<Renderer>();
		
		StartCoroutine(UpdateMouse());
	}
	
	void Update() {
		if(Input.GetMouseButtonDown(0))
			leftmouseClicked = true;
		if(Input.GetMouseButtonDown(1))
			rightmouseClicked = true;
		
		if(Input.GetMouseButtonUp(0))
			leftmousePressed = true;
		if(Input.GetMouseButtonUp(1))
			rightmousePressed = true;
	}
	#endregion
	
	#region METHODS
	IEnumerator UpdateMouse()
	{
		while(Application.isPlaying)
		{
			if(refreshRate > 0)
				yield return new WaitForSeconds(refreshRate);
			else
				yield return null;
			
			Screen.showCursor = false;
			MouseConfig();
			CamCheck();
			CamRaycast();
			if(fixedToOwnerHeight==true && owner!=null)
			{
				Vector3 p =this.gameObject.transform.position;
				p.y=owner.transform.position.y;
				this.gameObject.transform.position=p;
			}
		}
	}
	
	void MouseConfig()
	{
		if(Screen.showCursor)
			Screen.showCursor = false;
	}
	
	void CamCheck()
	{
		if(mouseCam == null)
			mouseCam = GetComponent<Camera>();
		
		if(mouseCam == null)
			mouseCam = Camera.main;

		if(mouseCam != null)
		{
			MouseInteractionLayers = mouseCam.cullingMask;
			
			if(centerMouseOnCamera)
			{
				// Mouse stays at the center of the camera
				ray = new Ray(myTransform.position, myTransform.forward);
			}
			else
			{
				// Free Moving throughout the world
		    	ray = mouseCam.ScreenPointToRay (Input.mousePosition);
			}
		}
		else
			Debug.LogError("WorldMouse - mouseCam is not defined.  The script must be attached to a game object with a camera AND / OR the scene must have a camera with the \"MainCamera\" tag");
	}
	
	void CamRaycast()
	{
		if(MouseRootObject == null)
			return;
		
		if(Physics.Raycast(ray,out hit))//, LayerMask.NameToLayer("Ignore Raycast") | LayerMask.NameToLayer("Player")))
		{
			hitTransform = hit.transform;
			aimingPoint = hit.point;
		    MouseRootObject.position = new Vector3(aimingPoint.x,aimingPoint.y,aimingPoint.z);
		    MouseRootObject.rotation = new Quaternion(hit.normal.x, hit.normal.y, hit.normal.z, 1);
			
			if(lastHitTransform == null)
				lastHitTransform = hitTransform;
		}
		else
		{
			// Mouse is not colliding with an object, so let's just draw it in space
			aimingPoint = ray.GetPoint(maxReach);
		    MouseRootObject.position = new Vector3(aimingPoint.x,aimingPoint.y,aimingPoint.z);
			MouseRootObject.rotation = Quaternion.identity;
		}
		
		if(hitTransform != null)
		{
			// Hover is not a distance event by default
			HoverEvent();
			
			if(Vector3.Distance(MouseRootObject.position, myTransform.root.position) <= maxReach)
			{
				DistanceBasedEvents();
				GreenMaterial();
			}
			else
				RedMaterial();
		}
	}
	
	void HoverEvent()
	{
#if !UNITY_IPHONE || !UNITY_ANDROID
		// Not active in touch environments
		if(hitTransform.GetInstanceID() != lastHitTransform.GetInstanceID())
		{
			lastHitTransform.SendMessage("WorldMouseOnHover",false,SendMessageOptions.DontRequireReceiver);
			lastHitTransform = hitTransform;
		}
		hitTransform.SendMessage("WorldMouseOnHover",true,SendMessageOptions.DontRequireReceiver);
#endif
	}
	
	void DistanceBasedEvents()
	{
		if(leftmouseClicked)
		{
			hitTransform.SendMessage("WorldMouseOnLeftClick",SendMessageOptions.DontRequireReceiver);
			leftmouseClicked = false;
		}
		if(leftmousePressed)
		{
			hitTransform.SendMessage("WorldMouseOnLeftPress",SendMessageOptions.DontRequireReceiver);
			leftmousePressed = false;
		}
		
		if(rightmouseClicked)
		{
			hitTransform.SendMessage("WorldMouseOnRightClick",SendMessageOptions.DontRequireReceiver);
			rightmouseClicked = false;
		}
		if(rightmousePressed)
		{
			hitTransform.SendMessage("WorldMouseOnRightPress",SendMessageOptions.DontRequireReceiver);
			rightmousePressed = false;
		}
		
	}
	
	void GreenMaterial()
	{
		if(InteractiveObjectMaterial && MouseRenderer)
			MouseRenderer.sharedMaterial = InteractiveObjectMaterial;
	}
	
	void RedMaterial()
	{
		if(NothingOfInterestMaterial && MouseRenderer)
			MouseRenderer.sharedMaterial = NothingOfInterestMaterial;
	}
	#endregion
}