﻿using UnityEngine;
using System.Collections;

public class SkeletonController : MonoBehaviour {

	private Animator anim;
	
	void Start ()
	{
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.Space ))
		{
			anim.SetBool("isFiring", true );
		}
	}
}
